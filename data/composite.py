import os, sys
import subprocess
from subprocess import call

def add_watermark(orig_image):
	
	out_image = "../converted/out.jpg"

	# find image orientation
	p = subprocess.Popen("/opt/local/bin/identify -format '%[exif:orientation]' " + orig_image,
		shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
	for line in iter(p.stdout.readline, ''):
		orient_int = int(line)
		print orient_int

		if (orient_int == 1):
			print "landscape not supported"

		if (orient_int == 6):
			os.system( "composite -gravity west LumixLogo-L.png " + orig_image + " " + out_image );
			os.system( "composite -gravity east LumixPOST-L.png " + out_image  + " " + out_image );

		if (orient_int == 8):
			os.system( "composite -gravity east LumixLogo-R.png " + orig_image + " " + out_image );
			os.system( "composite -gravity west LumixPOST-R.png " + out_image  + " " + out_image );

	retval = p.wait()

