#!/usr/bin/python
import sys, os, glob, requests

# url to post to
# url = 'http://localhost/~gdunne/tmp/index.php'
url = 'http://localhost:8000'
context = 'lumix'
# log
out = open('/tmp/x', 'a')

def main():

	# get the source and destination folders from the script arguments
	src  = sys.argv[1]
	dest = sys.argv[2]

	# loop through all .jpg's in src folder
	for f in glob.glob(src + "*.jpg"):
		data  = { 'context' : context }
		files = { 'file' : open(f, 'rb') }
		r = requests.post(url, data=data, files=files)

		# move each file to dest location now that it's been posted
		os.rename(f, dest + os.path.basename(f));


if __name__ == "__main__":
    main()
