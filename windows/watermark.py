import os, sys
import subprocess
from subprocess import call

def add_watermark(orig_image, out_image):

	# find image orientation
	p = subprocess.Popen("identify -format '%[exif:orientation]' " + orig_image,
		shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
	#retval = p.wait()

	for line in iter(p.stdout.readline, ''):
		orient_int = int(line.replace("'", ""))

		# landscape
		if (orient_int == 1):
			i1 = subprocess.Popen("composite -gravity north ..\data\LumixLogo.png " + orig_image + " " + out_image,
			shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT);
			r = i1.wait()
			i2 = subprocess.Popen("composite -gravity south ..\data\LumixPOST.png " + out_image  + " " + out_image,
			shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT);
			r = i2.wait()

		# portrait 1
		if (orient_int == 6):
			i1 = subprocess.Popen("composite -gravity west ..\data\LumixLogo-L.png " + orig_image + " " + out_image,
			shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT);
			r = i1.wait()
			i2 = subprocess.Popen("composite -gravity east ..\data\LumixPOST-L.png " + out_image  + " " + out_image,
			shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT);
			r = i2.wait()

		# portrait 2
		if (orient_int == 8):
			i1 = subprocess.Popen("composite -gravity east ..\data\LumixLogo-R.png " + orig_image + " " + out_image,
			shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT);
			r = i1.wait()
			i2 = subprocess.Popen("composite -gravity west ..\data\LumixPOST-R.png " + out_image  + " " + out_image,
			shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT);
			r = i2.wait()

	
