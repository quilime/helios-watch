import os, shutil, time
import win32file
import win32event
import win32con
import requests
import watermark
import xml.etree.ElementTree as ET
from xml.etree.ElementTree import Element, SubElement, tostring

# SETTINGS
path_to_watch = os.path.abspath ("C:/fish/")
pictures_path = os.path.abspath ("C:/Users/i/Desktop/lumix_uploads/")
posted_path   = os.path.abspath ("C:/Users/i/Desktop/lumix_posts/")
post_url      = "https://panasonic.fishsoftware.com/module/post/"
context       = "lumix"


print '''
initializing watch
'''



# FindFirstChangeNotification sets up a handle for watching
#  file changes. The first parameter is the path to be
#  watched; the second is a boolean indicating whether the
#  directories underneath the one specified are to be watched;
#  the third is a list of flags as to what kind of changes to
#  watch for. We're just looking at file additions / deletions.
#
change_handle = win32file.FindFirstChangeNotification (
	path_to_watch, 0, win32con.FILE_NOTIFY_CHANGE_FILE_NAME
)


# Loop forever, listing any file changes. The WaitFor... will
#  time out every half a second allowing for keyboard interrupts
#  to terminate the loop.
#
try:
	old_path_contents = dict ([(f, None) for f in os.listdir (path_to_watch)])
	while 1:
		result = win32event.WaitForSingleObject (change_handle, 500)


		# If the WaitFor... returned because of a notification (as
		#  opposed to timing out or some error) then look for the
		#  changes in the directory contents.
		#
		if result == win32con.WAIT_OBJECT_0:
			new_path_contents = dict ([(f, None) for f in os.listdir (path_to_watch)])
			added = [f for f in new_path_contents if not f in old_path_contents]
			deleted = [f for f in old_path_contents if not f in new_path_contents]
			if added:
				if any("visitor.xml" in vis for vis in added):

					visitor_xml = path_to_watch + '/visitor.xml'
					tmp_session_xml = '..\data\session.xml'
					tree = ET.parse(visitor_xml)
					root = tree.getroot()

					for e in root.iter('id'):
						id = e.text
						break

					if (id):

						print "RFID: " + id

						print "Creating session.xml..."
						session_xml = Element('session')
						session_id = SubElement(session_xml, 'id')
						session_id.text = id
						f = open(tmp_session_xml, 'w')
						f.write(tostring(session_xml))
						f.close()

						# look for most recent image
						fileList = []
						for root, subFolders, files in os.walk(pictures_path):
						    for file in files:
						    	if str.lower(file).endswith(".jpg"):
						        	fileList.append(os.path.join(root, file))
						fileList.sort(key=lambda x: os.path.getctime(x))

						# if there are files in the folder
						if (fileList.__len__() >= 1) :
							most_recent_image = fileList[fileList.__len__() - 1]

							print "Adding watermark to " + most_recent_image
							watermarked_image = posted_path + "/" + os.path.splitext(os.path.basename(most_recent_image))[0] + "-w.JPG"
							watermark.add_watermark(most_recent_image, watermarked_image);

							print "Posting Image"
							post_data = { "context" : context, "tag_id_1" : id }
							post_files = { "image" : open(watermarked_image, "rb" ) }
							r = requests.post(post_url, data=post_data, files=post_files)
							print "server response " + str(r.status_code)
							print r.text

							# recursively delete images
							for root, dirs, files in os.walk(pictures_path):
								for file in files:
									if str.lower(file).endswith(".jpg"):
										os.remove(os.path.join(root, file))

						else :
							print "No Images in folder."

						print "Closing Session"
						shutil.copyfile(tmp_session_xml, path_to_watch + '\session.xml')
						os.remove(visitor_xml)

			old_path_contents = new_path_contents
			win32file.FindNextChangeNotification (change_handle)

finally:
	win32file.FindCloseChangeNotification (change_handle)
