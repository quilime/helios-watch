Windows Installation
-------------
1. Install Python 2.7
	http://www.python.org/getit/

2. Install Requests
	Download zip from: https://github.com/kennethreitz/requests
	move request-master/requests to C:/Python27/Lib/

3. Install ImageMagick from Binary Release
	http://www.imagemagick.org/script/binary-releases.php

4. Install Python win32 bindings
	http://starship.python.net/~skippy/win32/Downloads.html


Windows Usage
-----
1. Edit settings in watch.py. Defaults are:
	path_to_watch = os.path.abspath ("C:/fish/")
	pictures_path = os.path.abspath ("C:/Users/i/Desktop/lumix_uploads/")
	posted_path   = os.path.abspath ("C:/Users/i/Desktop/lumix_posts/")
	post_url      = "https://panasonic.fishsoftware.com/module/post/"
	context       = "lumix"

2. Open command window and navigate to /windows folder in project.

3. Run
>	python watch.py

4. Take picture with Panasonic camera, and send via WIFI.

5. Wait till picture is successfully uploaded via WIFI.

6. Scan RFID

7. GOTO 4 and repeat.





OSX Installation (for reference only -- incomplete)
------------
1. Install pip via brew (http://mxcl.github.com/homebrew/)  or
$	 easy_install pip

2. Install `Requests` library for Python:
	http://docs.python-requests.org/en/latest/user/install/#install
	or
$	sudo pip install requests

3. Edit locations in com.helios.watchfolder.plist

4. Place com.helios.watchfolder.plist file in one of the following locations:
	~/Library/LaunchAgents         Per-user agents provided by the user.
	/Library/LaunchAgents          Per-user agents provided by the administrator.
	/Library/LaunchDaemons         System-wide daemons provided by the administrator.
	/System/Library/LaunchAgents   Per-user agents provided by OS X.
	/System/Library/LaunchDaemons  System-wide daemons provided by OS X.

5. Start watch daemon:
	load watch script
$	launchctl load com.helios.watchfolder.plist

	unload watch script
$	launchctl unload com.helios.watchfolder.plist
